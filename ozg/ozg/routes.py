def includeme(config):

    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('todo', '/todo')
    config.add_route('kontakte', '/kontakte')
    config.add_route('impress', '/impressum')
    config.add_route('obf', '/obf')
    config.add_route('oof', '/oof')
    config.add_route('ozgtext', '/ozgtext')
    config.add_route('about-us', '/how-we-are')
    config.add_route('about-idee', '/projektidee')
