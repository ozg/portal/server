import pytest
import re
from datetime import date
from pydantic import BaseModel, ValidationError, validator


class FormData(BaseModel):
    """Werte aus dem Formular"""

    datum: str = '00-00-0000'

    @validator('datum')
    def check_datum(cls, v):
        tag, monat, jahr = v.split('-')
        try:
            datobj = date(int(jahr), int(monat), int(tag))
        except ValueError:
            raise ValueError('Das Format für das Datum: DD-MM-JJJJ')
        return v.title()


def test_portaldemo_validator_datum_falsch():

    with pytest.raises(ValidationError) as exc_info:
        FormData(datum='2020-12-12')
    assert exc_info.value.errors() == [
        {'loc': ('datum',),
         'msg': 'Das Format für das Datum: DD-MM-JJJJ',
         'type': 'value_error'}
    ]


def test_portaldemo_FromData():
    fd = FormData()
    assert fd.dict()['datum'] == '00-00-0000'


def test_portaldemo_FormData_datum_ok():
    fd = FormData(datum='01-12-2020')
    assert fd.dict() == {'datum': '01-12-2020'}


if __name__ == '__main__':
    pass
