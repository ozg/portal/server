# Import the 'modules' that are required for execution
import pytest
import pytest_html
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options as FirefoxOptions

#  Fixture for Firefox
@pytest.fixture(scope="class")
def driver_init(request):
    ff_options = FirefoxOptions()
    ff_options.add_argument("--headless")
    #  ff_driver = webdriver.Firefox(options=ff_options)
    ff_driver = webdriver.Firefox()
    request.cls.driver = ff_driver
    yield
    ff_driver.close()

#  Fixture for Chrome
#  @pytest.fixture(scope="class")
#  def chrome_driver_init(request):
#    chrome_options = ChromeOptions()
#    chrome_options.add_argument("--disable-extensions")
#    chrome_options.add_argument("--disable-gpu")
#    chrome_options.add_argument("--headless")
#    chrome_driver = webdriver.Chrome(options=chrome_options)
#    request.cls.driver = chrome_driver
#    yield
#    chrome_driver.close()


@pytest.mark.usefixtures("driver_init")
class BasicTest:
    pass


class Test_URL(BasicTest):

    def test_open_url(self):
        self.driver.get("http://localhost:6543/f11n/portaldemo/formdemo1")
        assert 'OZG-Portal' in self.driver.title

    def test_title(self):
        self.driver.get("http://localhost:6543/f11n/portaldemo/formdemo1")
        assert 'Daten als Dictionary verarbeiten' in self.driver.page_source

    def test_formular1(self):
        self.driver.get('http://localhost:6543/f11n/portaldemo/formdemo1')
        farbe = self.driver.find_element_by_id('farbe')
        farbe.send_keys("regenbogenfarben")
        time.sleep(3)
        essen = self.driver.find_element_by_id('essen')
        essen.send_keys("Hefeklöße mit Blaubeeren")
        time.sleep(3)
        essen.send_keys(Keys.RETURN)
        time.sleep(3)
        x = self.driver.page_source
        result = "{'anrede': 'frau', 'farbe': 'regenbogenfarben', "
        result += "'essen': 'Hefeklöße mit Blaubeeren', 'senden': 'Senden'}"
        assert result in x

    def test_checkboxes(self):
        self.driver.get('http://localhost:6543/f11n/portaldemo/formdemo1')
        farbe = self.driver.find_element_by_id('farbe')
        farbe.send_keys("rot")
        box1 = self.driver.find_element_by_id('checkbox_1').click()
        time.sleep(3)
        box2 = self.driver.find_element_by_id('checkbox_2').click()
        time.sleep(3)
        box3 = self.driver.find_element_by_id('checkbox_3').click()
        time.sleep(3)
        senden = self.driver.find_element_by_name('senden')
        time.sleep(3)
        senden.send_keys(Keys.RETURN)
        time.sleep(3)
        x = self.driver.page_source
        result = "{'anrede': 'frau', 'farbe': 'rot', 'essen': '', "
        result += "'checkbox_1': 'OZG', 'checkbox_2': "
        result += "'FIM', 'checkbox_3': 'value 3', "
        result += "'senden': 'Senden'}"
        assert result in x
