# Import the 'modules' that are required for execution
import pytest
import pytest_html

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options as FirefoxOptions

#  Fixture for Firefox
@pytest.fixture(scope="class")
def driver_init(request):
    ff_options = FirefoxOptions()
    ff_options.add_argument("--headless")
    ff_driver = webdriver.Firefox(options=ff_options)
    request.cls.driver = ff_driver
    yield
    ff_driver.close()


#  Fixture for Chrome
#  @pytest.fixture(scope="class")
#  def chrome_driver_init(request):
#    chrome_options = ChromeOptions()
#    chrome_options.add_argument("--disable-extensions")
#    chrome_options.add_argument("--disable-gpu")
#    chrome_options.add_argument("--headless")
#    chrome_driver = webdriver.Chrome(options=chrome_options)
#    request.cls.driver = chrome_driver
#    yield
#    chrome_driver.close()

@pytest.mark.usefixtures("driver_init")
class BasicTest:
    pass


class Test_URL(BasicTest):

    def test_open_url(self):
        self.driver.get("http://localhost:6543/f11n/portaldemo/index")
        assert 'OZG-Portal' in self.driver.title

    def test_title(self):
        self.driver.get("http://localhost:6543/f11n/portaldemo/index")
        assert 'gitlab.com' in self.driver.page_source
