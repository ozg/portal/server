import pytest
from ozg.f11n.portaldemo.dict2xml import DictToXML


def test_convert():
    "Dummy-Daten"

    example = {'sibling': {
        'couple': {
            'mother': 'mom',
            'father': 'dad',
            'children': [
                {'child': 'foo'},
                {'child': 'bar'}
            ]}
    }
    }
    xml = DictToXML(example)
#    print(xml.display())
    assert xml.get_string() == """<?xml version="1.0" ?>
<sibling>
  <couple>
    <mother>mom</mother>
    <father>dad</father>
    <children>
      <item>
        <child>foo</child>
      </item>
      <item>
        <child>bar</child>
      </item>
    </children>
  </couple>
</sibling>
"""


def test_formdata():
    "Beispieldaten vom Formular"

    example = {'formdata': {
        'datum': '14-12-2019',
        'senden': 'Senden',
        'anrede': 'frau',
        'farbe': 'lila',
        'essen': 'Hefeklöße',
        'checkbox_1': 'OZG',
        'checkbox_2': 'FIM',
        'checkbox_3': 'FITKO',
        'topic': 'Teil 2',
        'stylesheet': 'styles.css',
        'action': 'multiform3'
    }
    }
    xml = DictToXML(example)
    assert xml.get_string() == """<?xml version="1.0" ?>
<formdata>
  <datum>14-12-2019</datum>
  <senden>Senden</senden>
  <anrede>frau</anrede>
  <farbe>lila</farbe>
  <essen>Hefeklöße</essen>
  <checkbox_1>OZG</checkbox_1>
  <checkbox_2>FIM</checkbox_2>
  <checkbox_3>FITKO</checkbox_3>
  <topic>Teil 2</topic>
  <stylesheet>styles.css</stylesheet>
  <action>multiform3</action>
</formdata>
"""
