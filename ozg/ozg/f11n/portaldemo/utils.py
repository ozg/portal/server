from pyramid.path import AssetResolver
resolver = AssetResolver()


def get_package_name():
    "Get package name"

    name = resolver.resolve('portaldemo').pkg_name
    return name
