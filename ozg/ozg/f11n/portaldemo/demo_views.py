from pyramid.view import view_config
from .utils import get_package_name
from pyramid.renderers import render
from pyramid.response import Response
import json

from datetime import datetime
from .model_form1 import check_form1


@view_config(route_name='portaldemo',
             renderer='./templates/demostart.jinja2')
def portal_demo(request):
    return {'project': 'Portal-Demo'}


@view_config(route_name='portalformdemo1',
             renderer='./templates/formdemo1.jinja2')
def portal_formdemo1(request):
    """ Liefert ein Formuala aus

    Die Verarbeitung der Formulardaten erfolgt
    in der View: portalformdemo1data
    """   
    return {'topic': 'Daten als Dictionary verarbeiten',
            'action': 'formdemo1data'}


@view_config(route_name='portalformdemo1data')
def portal_formdemo1data(request):
    """ Testausgabe der uebertragenen Formulardaten """
    data = {}
    
    data.update(request.POST)
    fehler = check_form1(data)
    result = render('./templates/formdemo1data.jinja2',
                    {'data': data,
                     'fehler': fehler}, 
                     request=request)
    
    return Response(result)

@view_config(route_name='portalformdemo2',
             renderer='./templates/formdemo1.jinja2')
def portal_form2json(request):
    """ Liefert ein Formuala aus

    Die Verarbeitung der Formulardaten erfolgt
    in der View: portalformdemo1data
    """
    return {'topic': 'Formulardaten als JSON zurückgeben',
            'action': 'form2json'}

@view_config(route_name='json_api',
             renderer='json')
def form2json(request):
    """ Liefert die Formulardaten im JSON-Format aus

    Gleichzeitig werden die Daten in eine Datei geschrieben.
    """
    data = {}
    data.update(request.POST)
    return data

    # Ausgabe in eine Datei
    # with open('formdata.txt', 'w', encoding='utf8') as json_file:
    #     json.dump(data, json_file, ensure_ascii=False)
    
    
@view_config(route_name='portalformdemo3',
             renderer='./templates/formdemo3.jinja2')
def portal_formdemo3(request):
    """ Liefert ein Formualar aus

    Die Verarbeitung der Formulardaten erfolgt
    in der View: portalformdemo3data
    """
    return {'topic': 'Weitere Formularvarianten',
            'action': 'formdemo3data'}


@view_config(route_name='portalformdemo3data')
def portal_formdemo3data(request):
    """    """

    data = {}
    data.update(request.POST)
    
    # das Ergebins rendern
    result = render('./templates/formdemo3data.jinja2',
                    {'data': data},
                    request=request)

    return Response(result)
