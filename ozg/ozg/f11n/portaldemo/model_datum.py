from pydantic import BaseModel, ValidationError, validator
import re
import json
from datetime import date


class FormData(BaseModel):
    """ Werte, die wir erwarten

       Der Wert für das Feld farbe ist hier ein Pflichtfeld.
    """
    datum: str = ''

    @validator('datum')
    def check_datum(cls, v):
        try:
            tag, monat, jahr = v.split('-')
            datobj = date(int(jahr), int(monat), int(tag))
        except:
            raise ValueError('Das Format für das Datum: DD-MM-JJJJ')
        return v.title()

    @validator('datum')
    def check_datum_length(cls, v):
        tag, monat, jahr = v.split('-')
        if len(jahr) != 4:
            raise ValueError('''Das Format für das Datum: DD-MM-JJJJ,
            Jahresangabe mit vier Stellen bitte!''')
        return v.title()

    @validator('datum')
    def check_datum_regexp(cls, v):
        pattern = "^[0-3]?[0-9][-][0-3]?[0-9][-](?:[0-9]{2})?[0-9]{2}$"
        if not re.search(pattern, v):
            raise ValueError('Das Format für das Datum: DD-MM-JJJJ (gefunden durch Regulären Ausdruck)')
        return v.title()

    
def check_datum(data):
    fehler = []
    try:
        FormData(**data)
    except ValidationError as e:
        fehler = json.loads(e.json())

    return fehler


if __name__ == '__main__':

    print('01-01-2019 <-- Das ist OK!')
    data = {'datum': '01-01-2019'}
    print(check_datum(data))
    print('01.01.2019 <-- Das geht schief!!!')
    data = {'datum': '01.01.2019'}
    print(check_datum(data))
    print('12.24.2020 <-- Das geht schief!!!')
    data = {'datum': '12.24.2020'}
    print(check_datum(data))
