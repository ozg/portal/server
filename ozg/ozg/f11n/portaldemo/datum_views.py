""" Demo fuer die Behandlung von Datumangaben"""
from pyramid.view import view_config
from pyramid.renderers import render
from pyramid.response import Response

from pydantic import BaseModel, ValidationError, validator
from .model_datum import check_datum

from datetime import datetime


@view_config(route_name='portalformddemo_datum')
def handle_datum(request):
    """Liefert ein Datum an das Formular

    und validiert die korrekte Eingabe.
    """
    data = {}
    fehler = None
    data.update(request.POST)
    if data.get('datum', ''):
        inputdata = {'datum': data['datum']}
        data['fehler'] = check_datum(inputdata)
    else:
        d = datetime.now()
        heute = f"{d.day:02}-{d.month:02}-{d.year:04}"
        data['datum'] = heute
        result = render('./templates/formdemo_datum.jinja2',
                        {'topic': 'Demo: Datum',
                         'data': data,
                         'fehler': None},
                        request=request)
        return Response(result)

    if data.get('fehler'):
        result = render('./templates/formdemo_datum.jinja2',
                        {'topic': 'Demo: Datum ist nicht OK',
                         'data': data,
                         'fehler': fehler},
                        request=request)
        return Response(result)
    else:
        result = render('./templates/formdemo_datum_data.jinja2',
                        {'topic': 'Demo: Datum ist OK',
                         'data': data,
                         'fehler': fehler},
                        request=request)
        return Response(result)
