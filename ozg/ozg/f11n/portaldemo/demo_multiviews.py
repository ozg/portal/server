from pyramid.view import view_config
from .utils import get_package_name
from pyramid.renderers import render
from pyramid.response import Response
import json

from datetime import datetime
from .model_form1 import check_form1
from .dict2xml import DictToXML


@view_config(route_name='multiformpart1',
             renderer='./templates/multiformdemo1.jinja2')
def multiformpart1(request):
    """ Liefert ein Formuala aus

    Die Daten werden an multiform2 weitergereicht
    """
    datacollection = {}
    datacollection.update(request.POST)
    for k, v in datacollection.items():
        datacollection[k] = v
    datacollection['topic'] = 'Daten einsammeln und als XML speichern'
    return {'data': datacollection,
            'action': 'multiform2'}

@view_config(route_name='multiformpart2',
             renderer='./templates/multiformdemo2.jinja2')
def multiformpart2(request):
    """ Liefert ein Formualardaten aus

    Die Verarbeitung der Formulardaten erfolgt
    in der View: formdemo3data
    """
    datacollection = {}

    datacollection.update(request.POST)
    datacollection.update({'topic': 'Formulardaten Teil 2',
                           'action': 'multiform3'})
    return {'data': datacollection}


@view_config(route_name='multiformpart3')
def multiformpart3(request):
    """ Test-Ausgabe in diversen Formaten

    - als Dictionary
    - als XML
    - in for-Schleife als Liste
    """
    data = {}
    data.update(request.POST)
    print(data)
    formdata = data
    # zusätzliches Root-Element einfuegen
    demoportal = {'demoportal': formdata}
    xml = DictToXML(demoportal)
    xml2string = xml.get_string()
    # das Ergebins rendern
    result = render('./templates/multiformdemo3.jinja2',
                    {
                        'data': data,
                        'xml': xml2string
                    },
                    request=request)
    return Response(result)
