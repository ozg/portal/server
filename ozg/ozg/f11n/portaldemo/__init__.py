
def includeme(config):
    config.add_jinja2_search_path("ozg:templates")
    config.add_route('portaldemo', '/f11n/portaldemo/index')
    config.add_route('portalformdemo1', '/f11n/portaldemo/formdemo1')
    config.add_route('portalformdemo1data', '/f11n/portaldemo/formdemo1data')
    config.add_route('portalformdemo2', '/f11n/portaldemo/formdemo2')

    config.add_route('json_api', '/f11n/portaldemo/form2json')

    config.add_route('portalformdemo3', '/f11n/portaldemo/formdemo3')
    config.add_route('portalformdemo3data', '/f11n/portaldemo/formdemo3data')

    config.add_route('portalformddemo_datum', '/f11n/portaldemo/form-datum')

    config.add_route('multiformpart1', '/f11n/portaldemo/multiform1')
    config.add_route('multiformpart2', '/f11n/portaldemo/multiform2')
    config.add_route('multiformpart3', '/f11n/portaldemo/multiform3')

    config.add_static_view('static_portaldemo', 'static', cache_max_age=3600)
    config.scan()
