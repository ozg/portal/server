import unittest

from pyramid import testing


class ViewTests(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def test_my_view(self):
        from ozg.views.default import startpage
        request = testing.DummyRequest()
        info = startpage(request)
        self.assertEqual(info['topic'], 'Portal-Demo')


class FunctionalTests(unittest.TestCase):
    def setUp(self):
        from ozg import main
        app = main({})
        from webtest import TestApp
        self.testapp = TestApp(app)

    def test_root(self):
        res = self.testapp.get('/', status=200)
        self.assertTrue(b'Portal-Demo' in res.body)

    def test_menu_fachverfahren(self):
        res = self.testapp.get('/', status=200)
        self.assertTrue(b'Fachverfahren' in res.body)
