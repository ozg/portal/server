from pyramid.view import view_config


@view_config(route_name='about-idee',
             renderer='../templates/about-idee.jinja2')
def about_idee_page(request):
    return {'topic': 'OZG: Idee'}


@view_config(route_name='ozgtext',
             renderer='../templates/ozgtext.jinja2')
def ozg_text(request):
    return {'topic': 'Portal-Demo: OZG-Text'}

@view_config(route_name='about-us',
             renderer='../templates/about-us.jinja2')
def about_us_page(request):
    return {'topic': 'OZG'}

@view_config(route_name='home',
             renderer='../templates/startpage.jinja2')
def startpage(request):
    return {'topic': 'Portal-Demo'}

@view_config(route_name='kontakte',
             renderer='../templates/kontakt.jinja2')
def get_kontakte(request):
    return {'topic': 'Kontakt'}

@view_config(route_name='impress',
             renderer='../templates/impressum.jinja2')
def get_impress(request):
    return {'topic': 'Impressum'}

@view_config(route_name='obf', renderer='../templates/fachverfahren.jinja2')
@view_config(route_name='oof', renderer='../templates/todo.jinja2')
@view_config(route_name='todo', renderer='../templates/todo.jinja2')
def todo_page(request):
    return {'project': 'ozg'}

