# Server

Pyramid app  serving  collections of OZG apps.

- Clone and create a new a created project.

    ```
    git clone https://gitlab.com/ozg/portal/server.git portal
    cd portal
    ```
    
- Create a Python virtual environment.

    ```
    python3 -m venv env
    ```
    
- Activate your new environment.

    ```
    ./env/bin/activate
    cd ozg
    ```
    
- Upgrade packaging tools.

    ```
    pip install --upgrade pip setuptools
    ```
    
- Install the project in editable mode with its testing requirements.

    ```
    pip install -e ".[testing]"
    ```
    
- Run your project's tests (without selenium).

    ```
     pytest --ignore=ozg/f11n/portaldemo/tests/selenium 

    ```
- Run your test's with selenium

  You have to download and install »geckodriver« from:

  https://github.com/mozilla/geckodriver/releases   
  
- Run your project.

    ```
    pserve development.ini
    ```
